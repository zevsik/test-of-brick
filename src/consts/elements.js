export const MIN_COUNT_LENGTH = 1;
export const MAX_COUNT_LENGTH = 100;
export const MIN_ROWS_LENGTH = 1;
export const MAX_ROWS_LENGTH = 40;
export const HEIGHT_OF_ROWS = 35;
export const FULL_ROWS_HEIGHT = '100%';
