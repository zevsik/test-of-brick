export default {
  actions: {
    async fetchElements({ commit }, limit = 3) {
      const res = await fetch(
        `https://jsonplaceholder.typicode.com/comments?_limit=${limit}`,
      );
      const elements = await res.json();

      commit('updateElements', elements);
    },

    async fetchClear({ commit }) {
      commit('clearElements');
    },
  },
  mutations: {
    updateElements(state, elements) {
      state.elements = elements;
    },
    clearElements(state) {
      state.elements = state;
    },
  },
  state: {
    elements: [],
  },
  getters: {
    validElements(state) {
      return state.elements.filter((p) => p.email);
    },
    allPosts(state) {
      return state.elements;
    },
    elementsCount(state, getters) {
      return getters.validElements.length;
    },
  },
};
