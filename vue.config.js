const fs = require('fs');
const webpack = require('webpack');
const sass = require('sass');

const packageJson = fs.readFileSync('./package.json');
const { version } = JSON.parse(packageJson);

if (!version) {
  throw new Error('Has no version in package json');
}

module.exports = {
  configureWebpack: {
    entry: {
      app: './src/main.js',
    },
    plugins: [
      new webpack.DefinePlugin({
        PACKAGE_VERSION: `"${version}"`,
      }),
    ],
  },
  chainWebpack: (config) => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap((options) => {
        // eslint-disable-next-line no-param-reassign
        options.compilerOptions.whitespace = 'preserve';
        return options;
      });
  },

  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import '@/styles/utils/_variables.scss';
        `,
        implementation: sass,
      },
    },
    extract: false,
  },
  pluginOptions: {
    lintStyleOnBuild: false,
    stylelint: {},
  },
};
