# Тестовое задание

###

Реализовать компонент с использованием библиотеки `VueJS` для отображения списка однородных элементов.
Количество элементов неограниченно (в разумных пределах). Область, занимаемая компонентом, ограничивается 
несколькими строчками (задается параметром). Если все элементы в область не влезают, 
отображается кнопка «показать все». Нажатие на кнопку приводит к раскрытию всего списка.

Использовать:
- `Webpack (vue-cli)`
- `Vuex`
- `Vuevadate`
- `Sass`
- Компонентный подход
- Использовать `js/css` линтеры


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
